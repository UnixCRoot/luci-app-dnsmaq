include $(TOPDIR)rules.mk

PKG_NAME:=luci-app-dnsmasq
PKG_VERSION:=0.0.1
PKG_MAINTAINER:=CRoot <croot@xcroot.com>

PKG_BUILD_DIR:=$(BUILD_DIR)/$(PKG_NAME)

include $(INCLUDE_DIR)/package.mk

define Package/$(PKG_NAME)
	CATEGORY:=LuCI
	SUBMENU:=3. Applications
	TITLE:=LuCI support for dnsmasq
	PKGARCH:=all
	DEPENDS:=+dnsmasq-full
	MAINTAINER:=CRoot
endef

define Package/$(PKG_NAME)/description
    A LuCI support for dnsmasq
endef

define Package/$(PKG_NAME)/postinst
endef

include ....luci.mk