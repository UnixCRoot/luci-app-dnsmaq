
-- Auther CRoor <croot@xcroot.com>

module("luci.controller.dnsmasq.index", package.seeall)

function index()
    entry({"admin", "services", "dnsmasq"}, template("dnsmasq/index"), _("域名绑定"), 60).dependent =false
    entry({"admin", "services", "dnsmasq_save"}, call("save"),nil).dependent =false
end

function save()
    local http = require "luci.http"
    local sys = require "luci.sys"
    local body,len = http.content()
    local servers = http.formvalue("servers","nothing")
    local hosts = http.formvalue("hosts","nothing")
    http.prepare_content("application/json")

    nixio.fs.writefile("/etc/dnsmasq.servers", servers)
    nixio.fs.writefile("/etc/hosts", hosts)
    sys.exec("uci set dhcp.@dnsmasq[0].serversfile=\"/etc/dnsmasq.servers\"")
    sys.exec("uci commit")
    sys.exec("/etc/init.d/dnsmasq restart")
    http.write('{"status":"ok"}')
end